var mysql   = require('mysql');
var db  = require('./db_connection.js');
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT p.*, team_name FROM player p LEFT JOIN team_players tp ON tp.player_id = p.player_id AND tp.currently_on = 1 LEFT JOIN team t ON t.team_id = tp.team_id ORDER BY p.player_id';
    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

// DELETES PLAYER FROM DIFFERENT TABLES //
exports.retire = function(player_id, callback) {
    var query = 'DELETE FROM team_players WHERE player_id = ?';
    var queryData = [player_id];

    connection.query(query, queryData, function(err, result) {

        var query = 'DELETE FROM player_stats WHERE p_id = ?';
        var queryData = [player_id];

        connection.query(query, queryData, function(err, result){

            var query = 'DELETE FROM player WHERE player_id = ?';
            var queryData = [player_id];
            connection.query(query, queryData, function(err, result) {
                callback(err, result);

            })
        });
    });
};

// INSERTS DATA INTO MULTIPLE TABLES //
exports.insertT = function(params, callback) {

    var query = 'INSERT INTO player (p_firstname, p_lastname, position) VALUES (?, ?, ?)';

    var queryData = [params.first_name, params.last_name, params.position];

    connection.query(query, queryData, function(err, result) {

        var player_id = result.insertId;

        var query = 'INSERT INTO team_players (team_id, player_id, currently_on) VALUES (?, ?, ?)';

        var queryData = [params.team_id, player_id, params.status];
        connection.query(query, queryData, function(err, result){
            callback(err, result);
        });
    });
};