var mysql   = require('mysql');
var db  = require('./db_connection.js');
var connection = mysql.createConnection(db.config);

exports.getStats = function(team_id, callback) {
    var query = 'SELECT ts.*, stat_type, team_name FROM team_stats ts JOIN stats s ON s.stat_id = ts.stat_id JOIN team t ON t.team_id = ts.team_id WHERE ts.team_id = ?';
    var queryData = [team_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.update = function(params, callback) {
    var query = 'UPDATE team_stats SET current_season = ? WHERE stat_id = ? AND team_id = ?';
    var querydata = [params.current, params.stat_id, params.team_id];
    connection.query(query, querydata, function(err, result) {
        callback(err, result);
    });
};