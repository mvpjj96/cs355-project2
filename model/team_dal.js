/**
 * Created by johnm on 5/16/2017.
 */
var mysql   = require('mysql');
var db  = require('./db_connection.js');
var connection = mysql.createConnection(db.config);

// USES A VIEW //
exports.getAll = function(callback) {
    var query = 'SELECT * FROM team_standings;';
    connection.query(query, function(err, result) {
        callback(err, result);
    });
};
// USES A STORED PROCEDURE //
exports.tRoster = function(team_id, callback) {
    var query = 'CALL getRoster(?)';
    var queryData = [team_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.remove = function(player_id, callback) {
    var query = 'UPDATE team_players SET currently_on = 0 WHERE player_id = ?';
    var queryData = [player_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};
// USES A SUB-QUERY //
exports.addQB = function(callback) {
    var query = 'SELECT p.player_id, p_firstname, p_lastname, salary FROM player p WHERE p.player_id NOT IN (SELECT player_id FROM team_players WHERE currently_on = TRUE) AND p.position = "QB"';
    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.addWR = function(callback) {
    var query = 'SELECT p.player_id, p_firstname, p_lastname, salary FROM player p WHERE p.player_id NOT IN (SELECT player_id FROM team_players WHERE currently_on = TRUE) AND p.position = "WR"';
    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.addCB = function(callback) {
    var query = 'SELECT p.player_id, p_firstname, p_lastname, salary FROM player p WHERE p.player_id NOT IN (SELECT player_id FROM team_players WHERE currently_on = TRUE) AND p.position = "CB"';
    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.addS = function(callback) {
    var query = 'SELECT p.player_id, p_firstname, p_lastname, salary FROM player p WHERE p.player_id NOT IN (SELECT player_id FROM team_players WHERE currently_on = TRUE) AND p.position = "S"';
    connection.query(query, function(err, result) {
        callback(err, result);
    });
};


exports.insert = function(params, callback) {
    var query = 'INSERT INTO team_players (team_id, player_id, currently_on) VALUES (?, ?, ?)';
    var querydata = [params.team_id, params.player_id, params.status];
    connection.query(query, querydata, function(err, result) {
        callback(err, result);
    });
};