var mysql   = require('mysql');
var db  = require('./db_connection.js');
var connection = mysql.createConnection(db.config);

exports.getStats = function(player_id, callback) {
    var query = 'SELECT ps.*, stat_type FROM player_stats ps JOIN stats s ON s.stat_id = ps.stat_id WHERE ps.p_id = ?;'
    var queryData = [player_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};