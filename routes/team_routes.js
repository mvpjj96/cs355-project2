/**
 * Created by johnm on 5/16/2017.
 */
var express = require('express');
var router = express.Router();
var team_dal = require('../model/team_dal');

router.get('/all', function(req, res) {
    team_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('team/teamViewAll', { 'result':result });
        }
    });

});

router.get('/viewRoster/', function(req, res){
    if(req.query.team_id == null) {
        res.send('team_id is null');
    }
    else {
        team_dal.tRoster(req.query.team_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('team/Roster', {'result': result[0]});
            }
        });
    }
});

router.get('/removeP', function(req, res){
    if(req.query.player_id == null) {
        res.send('player_id is null');
    }
    else {
        team_dal.remove(req.query.player_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                res.redirect(302, '/team/all');
            }
        });
    }
});

router.get('/addQB', function(req, res){
    if(req.query.team_id == null) {
        res.send('team_id is null');
    }
    else {
        team_dal.addQB(function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                res.render('team/addQB', {team_id: req.query.team_id,'result': result});
            }
        });
    }
});

router.get('/addWR', function(req, res){
    if(req.query.team_id == null) {
        res.send('team_id is null');
    }
    else {
        team_dal.addWR(function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                res.render('team/addWR', {team_id: req.query.team_id,'result': result});
            }
        });
    }
});

router.get('/addCB', function(req, res){
    if(req.query.team_id == null) {
        res.send('team_id is null');
    }
    else {
        team_dal.addCB(function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                res.render('team/addCB', {team_id: req.query.team_id,'result': result});
            }
        });
    }
});

router.get('/addS', function(req, res){
    if(req.query.team_id == null) {
        res.send('team_id is null');
    }
    else {
        team_dal.addS(function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                res.render('team/addS', {team_id: req.query.team_id,'result': result});
            }
        });
    }
});


router.get('/insert', function(req, res) {
    // simple validation
    if (req.query.player_id == null) {
        res.send('A player must be selected.');
    }
    else {
        team_dal.insert(req.query, function (err, result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                team_dal.tRoster(req.query.team_id, function (err, result) {
                    res.render('team/Roster', {'result': result[0], was_successful: true});
                });
            }
        });
    }
});
module.exports = router;