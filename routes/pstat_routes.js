var express = require('express');
var router = express.Router();
var team_dal = require('../model/team_dal');
var player_dal = require('../model/player_dal');
var tstat_dal = require('../model/tstat_dal');
var pstat_dal = require('../model/pstat_dal');

router.get('/choose', function(req, res) {
    player_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('pstat/choose', { 'result':result });
        }
    });

});

router.get('/playerStats', function(req, res){
    if(req.query.player_id == null) {
        res.send('player_id is null');
    }
    else {
        pstat_dal.getStats(req.query.player_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('pstat/Stats', {'result': result});
            }
        });
    }
});

module.exports = router;