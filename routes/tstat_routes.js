var express = require('express');
var router = express.Router();
var team_dal = require('../model/team_dal');
var player_dal = require('../model/player_dal');
var tstat_dal = require('../model/tstat_dal');

router.get('/choose', function(req, res) {
    team_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('tstat/choose', { 'result':result });
        }
    });

});

router.get('/teamStats', function(req, res){
    if(req.query.team_id == null) {
        res.send('team_id is null');
    }
    else {
        tstat_dal.getStats(req.query.team_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('tstat/Stats', {'result': result});
            }
        });
    }
});

router.get('/modify', function(req, res){
    if(req.query.team_id == null) {
        res.send('team_id is null');
    }
    else {
        tstat_dal.getStats(req.query.team_id, function (err, result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('tstat/modify', {team_id: req.query.team_id, 'result': result});
            }
        });
    }
});

router.get('/update', function(req, res) {
    if (req.query.stat_id == null) {
        res.send('A statistic type must be selected.');
    }
    else {
        tstat_dal.update(req.query, function (err, team_id) {
            if (err) {
                console.log(err);
                res.send(err);
            }
            else {
                team_dal.getAll(function (err, result) {
                    if (err) {
                        res.send(err);
                    }
                    else {
                        res.render('tstat/choose', {'result': result, was_successful: true});
                    }
                });
            }
        });
    }
});

module.exports = router;