var express = require('express');
var router = express.Router();
var team_dal = require('../model/team_dal');
var player_dal = require('../model/player_dal');

router.get('/all', function(req, res) {
    player_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('player/ViewAll', { 'result':result });
        }
    });

});

router.get('/retire', function(req, res){
    if(req.query.player_id == null) {
        res.send('player_id is null');
    }
    else {
        player_dal.retire(req.query.player_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                res.redirect(302, '/player/all');
            }
        });
    }
});

router.get('/addT', function(req, res){
    team_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('player/Add2Team', {'result': result});
        }
    });
});

router.get('/insertT', function(req, res){
    if(req.query.first_name == '') {
        res.send('First Name must be provided.');
    }
    else if(req.query.last_name == '') {
        res.send('Last Name must be provided');
    }
    else if(req.query.position == '') {
        res.send('Players position must be provided');
    }
    else if(req.query.team_id == null) {
        res.send('A team must be selected');
    }
    else {
        player_dal.insertT(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                player_dal.getAll(function(err, result){
                    if(err) {
                        res.send(err);
                    }
                    else {
                        res.render('player/ViewAll', { 'result':result, was_successful: true });
                    }
                });
            }
        });
    }
});


module.exports = router;